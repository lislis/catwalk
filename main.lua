math.randomseed(os.time())

local shader_code = [[
#define NUM_LIGHTS 32

struct Light {
    vec2 position;
    vec3 diffuse;
    float power;
};

extern Light lights[NUM_LIGHTS];
extern int num_lights;
extern vec2 screen;

const float constant = 1.0;
const float linear = 0.09;
const float quadratic = 0.032;

vec4 effect(vec4 color, Image image, vec2 uvs, vec2 screen_coords){
    vec4 pixel = Texel(image, uvs);

    vec2 norm_screen = screen_coords / screen;
    vec3 diffuse = vec3(0);

    for (int i = 0; i < num_lights; i++) {
        Light light = lights[i];
        vec2 norm_pos = light.position / screen;

        float distance = length(norm_pos - norm_screen) * light.power;
        float attenuation = 1.0 / (constant + linear * distance + quadratic * (distance * distance));

        diffuse += light.diffuse * attenuation;
    }

    diffuse = clamp(diffuse, 0.0, 1.0);
    return pixel * vec4(diffuse, 1.0);
}
]]

local shader = nil

lights = {}
cat = {}
cat.x = 0
cat.y = 220
cat.speed = 25
is_meow = false

local timer = nil

for i = 1, 8, 1 do
   rand = math.random()
   foo = nil
   if rand > 0.5 then
      foo = -1
   else
      foo = 1
   end
   tab = {}
   tab.x = i * 120 + 20
   tab.y = love.math.randomNormal(0.4, 200) + (i * 20 * foo)
   tab.color = {love.math.randomNormal(0.1, 0.3), 0.1, love.math.randomNormal(0.2, 0.6)}
   tab.power = love.math.randomNormal(0.8, 25)
   table.insert(lights, tab)
end

function love.load()
   shader = love.graphics.newShader(shader_code)
   meow = love.audio.newSource("assets/213135__taira-komori__cat6.mp3", "static")
   animation = newAnimation(love.graphics.newImage("assets/catsprite/catsprites.png"), 18, 15, 1.6)
   font = love.graphics.newFont("assets/DJGROSS.ttf", 36)
end

function love.update(dt)
   animation.currentTime = animation.currentTime + dt
   if animation.currentTime >= animation.duration then
      animation.currentTime = animation.currentTime - animation.duration
   end

   if cat.x > love.graphics.getWidth() then
      if not(is_meow) then
         love.audio.play(meow)
         is_meow = true
         timer = love.timer.getTime()
      else
         if (love.timer.getTime() - timer) > 1.5 then
            love.window.close()
         end
      end
   else
      cat.x = cat.x +  dt * cat.speed
   end

   for i = 1, #lights, 1 do
      if math.random() > 0.6 then
         rand = math.random()
         if rand > 0.9 then
            lights[i].power = lights[i].power + 3
         elseif rand < 0.1 then
            lights[i].power = lights[i].power - 3
         end
      end
   end
end

function love.draw()
   love.graphics.setFont(font)
   love.graphics.setShader(shader)

   shader:send("screen", {
                  love.graphics.getWidth(),
                  love.graphics.getHeight()
   })

   shader:send("num_lights", #lights)

   for i = 1, #lights, 1 do
      shader:send("lights["..i.."].position", {
                     lights[i].x,
                     lights[i].y
      })
      shader:send("lights["..i.."].diffuse", lights[i].color)
      shader:send("lights["..i.."].power", lights[i].power)
   end

   love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
   local spriteNum = math.floor(animation.currentTime / animation.duration * #animation.quads) + 1
   love.graphics.draw(animation.spriteSheet, animation.quads[spriteNum], cat.x, cat.y, 0, 4)
   love.graphics.setShader()
   love.graphics.setColor(0, 0, 0)
   love.graphics.print("CatWalk", 80, 80)
end

function newAnimation(image, width, height, duration)
   local animation = {}
   animation.spriteSheet = image;
   animation.quads = {};

   for y = 0, image:getHeight() - height, height do
      for x = 0, image:getWidth() - width, width do
         table.insert(animation.quads, love.graphics.newQuad(x, y, width, height, image:getDimensions()))
      end
   end

   animation.duration = duration or 1
   animation.currentTime = 0

   return animation
end
