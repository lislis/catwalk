# CATWALK

What happens when the cat is out of the bag? It goes for a walk. Probably.

![screencap](screencap.gif)


## Credits

### Code

Sprite animation based on [this tutorial](https://www.love2d.org/wiki/Tutorial:Animation).

Shader code based on [this tutorial](https://github.com/SkyVault/Love2DTutorialSeries).

### Assets

- Cat sprite based on https://opengameart.org/content/cat-sprites under CCO
- Meow sound [cat6.mp3](https://freesound.org/people/Taira%20Komori/sounds/213135/)  by [Taira Komori](https://freesound.org/people/Taira%20Komori/) under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
- Font is [DJ Gross](https://www.fontsquirrel.com/fonts/DJ-Gross) under [this license](https://www.fontsquirrel.com/license/DJ-Gross)
